# Cvent APIs Integration

This module provides a convenience wrapper around the Cvent APIs.  Only a
portion of the API calls have been implemented, and others will be added
as needed.  The library also provides a generic method to implement
any Cvent API not formally supported in this library.  (See below.)

## API Examples

This module provides a service class CventService that can be used procedurally
like this:

~~~~
  // Initialize a Cvent Client.
  $config = [
    'base_uri' = 'https://whatever.it.is',
    'token' = 'here_is_my_token',
    'stage_uri' = 'https://you.can.set.a.stage.uri.here',
  ];
  $cventClient = new \Autodesk\Cvent\CventClient($config);

  // Initialize Cvent Service
  $cvent = new \Autodesk\Cvent\CventService($cventClient, $logger);
  
  // Lookup Cvent user data by Oxygen ID
  $user = $cvent->getUserByOxygenId($userid);
~~~~

The Cvent service also provides a callApi method that can be used to call any
Cvent endpoint.

### Cvent API Support

This module provides methods to:

- List Available Events - `listEvents()`
- Retrieve session information - `getSession($session_id)`
- Retrieve user by Oxygen ID - `getUserByOxygenId($oxygen_id)`
- Retrieve user interests - `getUserInterests($oxygen_id)`
- Toggle a session interest - `toggleSessionInterest($oxygen_id, $session_id)`

Read methods will return an associative array of data.

For methods which return a singular entity, if that entity is not found the
method will return null. Any other errors will throw an exception.
