<?php

namespace Autodesk\Cvent\Exception;

use GuzzleHttp\Exception\RequestException;

/**
 * @file
 * Contains Autodesk\Cvent\Exception.
 *
 * This exception handler is dedicated to manage the Cvent API based http
 * data error codes. However it contains nothing, it might be helpful to
 * catch it.
 */
class CventClientResponseException extends RequestException {

}
