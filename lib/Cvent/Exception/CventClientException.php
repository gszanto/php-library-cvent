<?php

namespace Autodesk\Cvent\Exception;

/**
 * @file
 * Contains Autodesk\Cvent\Exception.
 *
 * This exception handler is dedicted to manage the Cvent API based http
 * respoonse error codes. However it contains nothing, it might be helpful to
 * catch it.
 *
 * As of this a child of RuntimeException we might want to specify its behavior.
 */
class CventClientException extends \RuntimeException {

}
