<?php

namespace Autodesk\Cvent\Exception;

/**
 * @file
 * Contains Autodesk\Cvent\Exception.
 *
 * This exception handler is dedicated to manage the special error results of
 * Cvent API.
 */
class CventServiceException extends \RuntimeException {

}
