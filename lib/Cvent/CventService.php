<?php

namespace Autodesk\Cvent;

use Autodesk\Cvent\Exception\CventClientException;
use Psr\Log\LoggerInterface;
use Autodesk\Cvent\Exception\CventServiceException;

/**
 * Class CventService.
 */
class CventService {

  /**
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * A CventClient instance.
   *
   * @var \Autodesk\Cvent\CventClient
   */
  private $client;

  /**
   * Cvent service constructor.
   *
   * @param \Autodesk\Cvent\CventClient $client
   * @param \Psr\Log\LoggerInterface|null $logger Logger interface
   */
  public function __construct(CventClient $client, LoggerInterface $logger = null) {
    $this->client = $client;
    $this->logger = $logger;
  }

  /**
   * An opened wrapper to make request to anywhere in the Cvent API. Use this
   * with caution! Or maybe use the managed methods of this service.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function callApi($endpoint, $params = []) {
    return $this->client->cventClientRequest($endpoint, $params);
  }

  /**
   * Request a list of all Conferences in an account.
   *
   * @param string $type
   *  Omit this parameter ot return only standard events.
   *  Use "template" to return only template events.
   *  Use "all" to return all events
   *
   * @return array|null
   *  An array of events

   * @throws \Exception
   *   Throws a general exception if there is an issue calling the API.
   */
  public function listEvents($type = null) {
    $endpoint = '/api/events.do';

    $params = [];
    if (!empty($type)) {
      $params = ['type' => $type];
    }

    $data = $this->client->getCventResults($endpoint, $params);

    return $data['events'];
  }

  /**
   * Request session information
   *
   * @param string $session_id
   *   The Cvent Session ID
   * @param string|null $event
   *   The optional guid for the event
 *
   * @return array
   *  An associative array containing session attributes or null if session not
   *  found-
   */
  public function getSession($session_id, $event = null) {
    $endpoint = '/api/sessionData.do';
    $params = [
      'sessionID' => $session_id,
      'event' => $event
    ];

    // We have a speciality here: If the error code is 12, we don't need to
    // throw excepction, it only means, that the event doesn't exist.
    try {
      $data = $this->client->getCventResults($endpoint, $params);
    }
    catch (CventClientException $e) {
      if ($this->client->getErrorCode() == '12') {
        // @todo: Can we use the logger here to generate more accurate message?
        return NULL;
      }
      else throw $e;
    }

    return $data['session-data'][0];
  }

  /**
   * Requests user data for the specified user
   *
   * @param string $oxygen_id
   *   The oxygen_id of the user we are looking for
   *
   * @return array|null
   *   An associative array containing user attributes
   *    or null if user not found
   * 
   * @throws \Exception
   *   Throws a general exception if there is an issue calling the API.
   */
  public function getUserByOxygenId($oxygen_id) {
    $endpoint = '/api/userData.do';
    $params = ['clientID' => $oxygen_id];

    try {
      $data = $this->client->getCventResults($endpoint, $params);
    }
    catch (CventClientException $e) {
      // User not found, but we don't want an exception at this time.
      if ($this->invalidUser()) {
        return NULL;
      }
      else {
        throw $e;
      }
    }
    return $data['user-data'][0];
  }

  /**
   * Requests event interests for the specified user.
   *
   * @param string $oxygen_id
   *   The oxygen_id of the user
   * @param string|null $event
   *   The optional guid for the event
   *
   * @return array|null
   *   An associative array containing interest data
   *    or null if user not found
   * @throws \Exception
   *   Throws a general exception if there is an issue calling the API.
   */
  public function getUserInterests($oxygen_id, $event = null) {
    $endpoint = '/api/interestData.do';
    $params = [
      'clientID' => $oxygen_id,
      'event' => $event
    ];

    try {
      $data = $this->client->getCventResults($endpoint, $params);
    }
    catch (CventClientException $e) {
      // User not found, but we don't want an exception at this time.
      if ($this->invalidUser()) {
        return NULL;
      }
      else {
        throw $e;
      }
    }

    return $data['interests'];
  }


  /**
   * Adds or removes session interests for a registrant
   * including person, session, or exhibitor interests.
   *
   * @param string $oxygen_id
   *   The oxygen_id of the user
   * @param string $session_id
   *   The Cvent Session ID of the session to add or remove
   * @param string|null $event
   *   The optional guid for the event
   *
   * @return string|null
   *   'Added' or 'Removed' as an interest
   * 
   * @throws \Exception
   *   Throws a general exception if there is an issue calling the API.
   */
  public function toggleSessionInterest($oxygen_id, $session_id, $event = null) {
    $endpoint = '/api/toggleInterest.do';
    $params = [
      'clientID' => $oxygen_id,
      'sessionID' => $session_id,
      'event' => $event
    ];

    $data = $this->client->getCventResults($endpoint, $params);

    return $data['operation'];
  }


  /**
   * @return bool
   */
  private function invalidUser() {
    // @todo: Is this a valid condition for every api endpoint? If so, let's move this into the client as a public method.
    return ($this->client->getErrorCode() == '1');
  }
}
