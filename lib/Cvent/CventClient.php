<?php

namespace Autodesk\Cvent;


use Autodesk\Cvent\Exception\CventClientException;
use Autodesk\Cvent\Exception\CventClientResponseException;
use Autodesk\Cvent\Exception\CventServiceException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;

/**
 * @file
 * Contains Autodesk\Cvent.
 */
class CventClient extends Client {

  /**
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * The Cvent base uri.
   *
   * @var string
   */
  private $base_uri;

  /**
   * The Cvent stage environment base uri.
   *
   * @var string
   */
  private $stage_uri;

  /**
   * The Cvent api token.
   *
   * @var string
   */
  private $token;

  /**
   * If your application has been compromised by the api's failure, set it true
   * to keep your app working. If it is true, every api request is suspended.
   *
   * @var bool
   */
  private $emergency;

  /**
   * Whether it is running on stage or not.
   *
   * @var bool
   */
  private $stage_mode;

  /**
   * The error code of a html success (200) api error.
   *
   * @var null
   */
  private $errorCode;

  /**
   * The error text of a html success (200) api error.
   *
   * @var null
   */
  private $errorText;

  /**
   * CventClient constructor.
   */
  public function __construct($config, LoggerInterface $logger = null) {
    // If base_uri is not set, we have to quit immediately.
    if (!isset($config['base_uri'])) {
      throw new CventClientException('The CventClient configuration does not contain the base_uri parameter.');
    }
    else {
      $this->base_uri = $config['base_uri'];
    }

    // The token is also a requirement, but needs some validation.
    // @todo: Do we have an alternative way to get auth?
    if (!isset($config['token'])) {
      throw new CventClientException('The CventClient configuration does not contain the token parameter.');
    }
    elseif (isset($config['token']) && !$this->validateToken($config['token'])) {
      throw new CventClientException('The token of the CventClient configuration is invalid.');
    }
    else {
      $this->token = $config['token'];
    }

    $this->stage_uri = isset($config['stage_uri']) ? $config['stage_uri'] : FALSE;
    $this->stage_mode = isset($config['stage_mode']) ? $config['stage_mode'] : FALSE;
    // Turn on the stage mode, if the config say so.
    $this->setStageMode($this->stage_mode);
    $this->emergency = isset($config['emergency']) ? $config['emergency'] : FALSE;
    $this->logger = $logger;
    $this->errorCode = NULL;
    $this->errorText = NULL;

    parent::__construct($config);
  }

  /**
   * This prevents any api calls if the application is in emergency mode.
   * 
   * Note! However this is a public method, you don't want to call this 
   * directly in your application.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function request($method, $uri = '', array $options = []) {
    if (!$this->emergency) {
      return parent::request($method, $uri, $options);
    }
    else {
      // Hmm, I don't like because the  purpose of the whole emergency management
      // is not to kill the application on an API shutdown.
      // @todo: Handle this better.
      throw new CventClientException('The Cvent api is in emergency mode.');
    }
  }

  /**
   * The main request method. Injects the necessary defaults, and starts a 'POST'
   * request to the endpoint.
   *
   * @param string $endpoint
   *   The endpoint to be called. Never use the full url! The full url is built
   *   by $this->base_uri, so '/api/events.do' is fair enough.
   * @param array  $params
   *   An array of parameters to pass to the api request. The possible values
   *   depend on the api specification.
   *
   * @return \GuzzleHttp\Psr7\Response
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see https://wiki.autodesk.com/download/attachments/532975149/Autodesk%20University%202018%20-%20Cvent%20Conference%20APIs%20v3.12%20%281%29.docx?version=1&modificationDate=1557849926475&api=v2
   *
   */
  public function cventClientRequest(string $endpoint, array $params) {
    $params['apiToken'] = $this->token;
    $params['format'] = 'JSON';
    $options = [
      'form_params' => $params,
    ];

    /** @var \GuzzleHttp\Psr7\Response $response */
    try {
      $response = $this->request('POST', $endpoint, $options);
    }
    catch (BadResponseException $e) {
      $response = Psr7\str($e->getResponse());
      if ($this->logger) {
        $this->logger->error(Psr7\str($e->getResponse()));
      }
      throw new CventClientResponseException('Error response returned from Cvent API.', $response->getStatusCode());
    }
    catch (RequestException $e) {
      if ($e->hasResponse()) {
        if ($this->logger) {
          $this->logger->error(Psr7\str($e->getResponse()));
        }
      }
      else {
        if ($this->logger) {
          $this->logger->error(Psr7\str($e->getRequest()));
        }
      }
      throw new CventClientResponseException('Error requesting Cvent API', $e->getCode());
    }

    return $response;
  }

  /**
   * The simpliest way to pull out results from Cvent. Every exceptions are
   * managed here so if the api client configuration is right, you'll get the
   * proper results - or error.
   *
   * @param string $endpoint
   * @param array $params
   *
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getCventResults(string $endpoint, array $params) {
     $response =  $this->cventClientRequest($endpoint, $params);
     return $this->decodeResponse($response);
  }

  /**
   * Switch to stage mode. This prevents to update the production data by
   * accident.
   *
   * @param bool $stage_mode
   */
  public function setStageMode(bool $stage_mode): void {
    if ($this->stage_uri) {
      $this->stage_mode = $stage_mode;

      if ($this->stage_mode) {
        $this->base_uri = $this->stage_uri;
      }
    }
  }

  /**
   * Decode the response body.
   *
   * @param \GuzzleHttp\Psr7\Response $response
   *
   * @return array
   *   The JSON parsed object as an associative array.
   */
  public function decodeResponse($response) {
    $status = $response->getStatusCode();
    if ($status == 200) {
      // JSON parse the body and return it.
      $body = $response->getBody();
      $decoded_body = json_decode((string) $body, TRUE);

      // Let's see, if the response comes with some ununual.
      $this->error($decoded_body);

      return $decoded_body;
    }

    return NULL;
  }

  /**
   * Set the emergency mode on, so every connection to the api will be refused.
   */
  public function emergency() {
    $this->emergency = TRUE;
  }

  /**
   * Check if the token is valid.
   *
   * @todo I'm not sure that the Cvent API provides any method to check this, but hope the best.
   *
   * @param string $token
   *
   * @return bool
   */
  private function validateToken(string $token) {
    // @todo: This is a forced true until we get answer about the token validation.
    return TRUE;
  }

  /**
   * A Cvent based error manager.
   * 
   * @param $data
   */
  private function error($data) {
    // Api calls may contain an embedded success/error code.
    if (array_key_exists('response', $data)) {
      $code = $data['response']['@code'];
      if ($code != '0') {
        $this->errorCode = $code;
        $this->errorText = $data['response']['#text'];
        $code = $this->errorCode;
        $text = $this->errorText;
        // @todo: Maybe any $code or $text should be sanitized, I'm not sure if any of them can have any possible user input.
        throw new CventClientException("API Error ($code) $text");
      }
    }
  }

  /**
   * Getter of the errorCode.
   */
  public function getErrorCode() {
    return $this->errorCode;
  }

  /**
   * Getter of the errorText-
   */
  public function getErrorText() {
    return $this->errorText;
  }

}
